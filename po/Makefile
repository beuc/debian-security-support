# Makefile for various po files.
# From libintl-perl (1.24-1)

# Copyright: (C) 2002-2015, Guido Flohr <guido.flohr@cantanea.com>
# Copyright: (C) 2016 Santiago Ruano Rincón <santiago@debian.org>

# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3, or (at your option) any
# later version.

# A copy of the GPL-3 can be found at /usr/share/common-licenses/GPL-3 on
# any Debian system.

srcdir = .

CATALOGS  = $(LINGUAS)
MO_FILES = $(addsuffix .gmo, $(LINGUAS))

MSGMERGE = msgmerge --width=72
MSGFMT   = msgfmt
XGETTEXT = xgettext --width=72
MSGCAT   = msgcat
MSGATTRIB = msgattrib --width=72
CATOBJEXT = .po

include PACKAGE

TD = $(strip $(TEXTDOMAIN))

default: help

all: $(TD).pot update-po update-mo install

help:
	@echo "Available targets:"
	@echo "  pot                       - remake master catalog"
	@echo "  update-po                 - merge po files"
	@echo "  update-mo                 - regenerate mo files"
	@echo "  install                   - install mo files"
	@echo "  all                       - all of the above"

POTFILES = $(srcdir)/POTFILES \
	$(shell cat $(srcdir)/POTFILES) 

pot: $(TD).pot 

clean:
	rm -f *~ *.bak *.gmo

$(TD).pot: $(POTFILES)
	$(XGETTEXT) --output=$(srcdir)/$(TD).pox --from-code=utf-8 \
		--language=Shell \
		--add-comments=TRANSLATORS: --files-from=$(srcdir)/POTFILES \
		--copyright-holder="$(COPYRIGHT_HOLDER)" \
		--msgid-bugs-address="$(MSGID_BUGS_ADDRESS)" && \
	rm -f $@ && mv $(TD).pox $@

install: $(MO_FILES)
	cd $(srcdir); \
	targetdir='../debian/debian-security-support/usr/share/locale/' \
	languages='$(LINGUAS)'; \
	for lang in $$languages; do \
		mkdir -p "$$targetdir/$$lang/LC_MESSAGES" || exit 1; \
		dest="$$targetdir/$$lang/LC_MESSAGES/$(TD).mo"; \
		cat="$$lang.gmo"; \
		echo "installing $$cat as $$dest"; \
		cp -f $$cat $$dest && chmod 644 $$dest || exit 1; \
	done

update-mo: $(MO_FILES)

update-po:
	$(MAKE) $(TD).pot
	cd $(srcdir); \
        catalogs='$(CATALOGS)'; \
        for cat in $$catalogs; do \
          cat=`basename $$cat`; \
          lang=`echo $$cat | sed 's/\$(CATOBJEXT)$$//'`; \
          mv $$lang.po $$lang.old.po; \
          echo "$$lang:"; \
          if $(MSGMERGE) $$lang.old.po $(TD).pot -o $$lang.po; then \
		    if test -f ../debian/po/$$lang.po ; then \
			  $(MSGCAT) --use-first -o $$lang.compendium.po ../debian/po/$$lang.po $$lang.po ;  \
			  $(MSGCAT) --use-first -o update.po $$lang.compendium.po $$lang.po ; \
			  $(MSGMERGE) update.po $(TD).pot | $(MSGATTRIB) --no-obsolete > $$lang.po ;\
			  rm update.po $$lang.compendium.po ; \
		    fi; \
            rm -f $$lang.old.po; \
          else \
            echo "msgmerge for $$cat failed!"; \
            rm -f $$lang.po; \
            mv $$lang.old.po $$lang.po; \
          fi; \
        done

.SUFFIXES:
.SUFFIXES: .po .gmo

.po.gmo:
	$(MSGFMT) --check --statistics --verbose -o $@ $<

