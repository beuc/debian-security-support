#!/bin/sh

#%# Copyright (C) 2014-2017 Christoph Biedl <debian.axhn@manchmal.in-ulm.de>
#%# License: GPL-2.0-only

# This codes duplicates "postinst configure", see #928968 why this has to be done...

set -e

if [ -f /usr/share/debconf/confmodule ] && \
   ( [ "$DEBCONF_USE_CDEBCONF" ] || perl -e "" 2>/dev/null )
then
    . /usr/share/debconf/confmodule
else
    exit 0
fi

USERNAME=debian-security-support
LIB_DIR="/var/lib/$USERNAME"

    # assert user

    if ! getent passwd "$USERNAME" > /dev/null; then
        adduser \
            --system --quiet \
            --home "$LIB_DIR" \
            --shell /bin/false --group \
            --gecos "Debian security support check" \
            "$USERNAME"
    fi

    # needed for schroots which copy /etc/passwd from the host
    if [ ! -d "$LIB_DIR" ]; then
        mkdir "$LIB_DIR"
        chown $USERNAME:$USERNAME "$LIB_DIR"
    fi

    if [ "$TMPDIR" ] && [ "$(stat --format=%a "$TMPDIR")" != '1777' ] ; then
        export TMPDIR='/tmp'
    fi

    TEMPDIR="$(mktemp --tmpdir --directory debian-security-support.postinst.XXXXX)"
    trap "rm -rf '$TEMPDIR'" EXIT

    # Closes: #824081
    cd /tmp

    MODES="ended limited earlyend"

    for MODE in $MODES ; do
        OUTPUT="$TEMPDIR/output"
        runuser "$USERNAME" --shell /bin/bash --command "
        check-support-status \
            --type $MODE \
            --no-heading \
            --status-db \"$LIB_DIR/security-support.semaphore\" \
        " >"$OUTPUT"

        if [ -s "$OUTPUT" ] ; then
            db_capb escape
            db_subst debian-security-support/$MODE MESSAGE "$(debconf-escape -e <"$OUTPUT")"
            db_input critical debian-security-support/$MODE || true
            db_go
            db_reset debian-security-support/$MODE
        fi
    done
