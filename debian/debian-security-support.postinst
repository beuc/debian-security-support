#!/bin/sh

set -e

if [ -f /usr/share/debconf/confmodule ] && \
   ( [ "$DEBCONF_USE_CDEBCONF" ] || perl -e "" 2>/dev/null )
then
    . /usr/share/debconf/confmodule
else
    exit 0
fi

USERNAME=debian-security-support
LIB_DIR="/var/lib/$USERNAME"

######################################################################
#                                                                    #
# WARNING: if you modify code here, you probably also need to modify #
#          ../check-support-status.hook                              #
#                                                                    #
######################################################################

case "$1" in
configure)
    # assert user

    if ! getent passwd "$USERNAME" > /dev/null; then
        adduser \
            --system --quiet \
            --home "$LIB_DIR" \
            --shell /bin/false --group \
            --gecos "Debian security support check" \
            "$USERNAME"
    fi

    # needed for schroots which copy /etc/passwd from the host
    if [ ! -d "$LIB_DIR" ]; then
        mkdir "$LIB_DIR"
        chown $USERNAME:$USERNAME "$LIB_DIR"
    fi

    if [ "$TMPDIR" ] && [ "$(stat --format=%a "$TMPDIR")" != '1777' ] ; then
        export TMPDIR='/tmp'
    fi

    TEMPDIR="$(mktemp --tmpdir --directory debian-security-support.postinst.XXXXX)"
    trap "rm -rf '$TEMPDIR'" EXIT

    MODES="ended limited earlyend"

    for MODE in $MODES ; do
        OUTPUT="$TEMPDIR/output"
        runuser "$USERNAME" --shell /bin/bash --command "
        check-support-status \
            --type $MODE \
            --no-heading \
            --status-db \"$LIB_DIR/security-support.semaphore\" \
        " >"$OUTPUT"

        if [ -s "$OUTPUT" ] ; then
            db_capb escape
            db_subst debian-security-support/$MODE MESSAGE "$(debconf-escape -e <"$OUTPUT")"
            db_input critical debian-security-support/$MODE || true
            db_go
            db_reset debian-security-support/$MODE
        fi
    done
    ;;
abort-upgrade|abort-remove|abort-deconfigure)
    ;;
triggered)
    ;;
*)
    echo "postinst called with unknown argument '$1'"
    exit 1
    ;;
esac

db_stop

#DEBHELPER#
exit 0
